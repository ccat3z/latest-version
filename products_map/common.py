import re

CHECK_NAMES = {
    '^7-Zip': '7z',
    '^Python 3\.[0-9\.]{1,}(?: \(64-bit\))*$': 'python3',
    'Java (?:SE Development Kit )*8 Update': 'jdk8',
    '^AutoHotkey': 'autohotkey',
    'calibre': 'calibre',
    '^Git version [0-9\.]{1,}': 'git'
}

CHECK_VERSIONS = {
    '^Python 3\.[0-9\.]{1,}(?: \(64-bit\))*$':
        lambda x: re.findall('(?<=Python )3\.[0-9\.]{1,}', x['DisplayName'])[0],
    'Java (?:SE Development Kit )*8 Update':
        lambda x: "1.8.0_" + re.findall('(?<=Update )[0-9]{1,}',
                                        x['DisplayName'])[0]
}

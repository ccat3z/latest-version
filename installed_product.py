import winreg
import re
import products_map.common as common
import urllib.request
import urllib.error
from urllib.parse import quote
from functools import lru_cache
import uuid

# api
API_URL = "http://version.c0ldcat.cc/{}?{}"

@lru_cache()
def get_version_from_check_name(name):
    req = urllib.request.Request(API_URL.format(quote(name), uuid.uuid4()));
    req.add_header('Pragma', 'no-cache')
    while True:
        try:
            return urllib.request.urlopen(req).read().decode()
        except urllib.error.HTTPError:
            return None
        except urllib.error.URLError:
            print("request error, try again")
            continue
        break

# reg
def mapkey(key, func = lambda x: x):
    result = []

    i=0
    while True:
        try:
            result.append(func(winreg.EnumKey(key, i)))
            i += 1
        except OSError:
            break

    return result

def mapvalue(key, func = lambda key, val, _: [key, val]):
    result = []

    i=0
    while True:
        try:
            result.append(func(*winreg.EnumValue(key, i)))
            i += 1
        except OSError:
            break

    return dict(result)

# apps
CHECK_NAMES = {}
CHECK_VERSIONS = {}

for products_map in [common]:
    CHECK_NAMES.update(common.CHECK_NAMES)
    CHECK_VERSIONS.update(common.CHECK_VERSIONS)

class AppDict(dict):
    def __missing__(self, key):
        try:
            if key == 'CheckVersion':
                self['CheckVersion'] = self['DisplayVersion']

                for (pattern, gen_version_from_app) in CHECK_VERSIONS.items():
                    if re.match(pattern, self['DisplayName']) != None:
                        self['CheckVersion'] = gen_version_from_app(self)
                        break

                return self['CheckVersion']
            elif key == 'DisplayVersion':
                f = self['VersionGenerator']
                return f(self['DisplayName'])
            elif key == 'LatestVersion':
                return get_version_from_check_name(self['CheckName'])
            elif key == 'CheckName':
                self['CheckName'] = None

                for (pattern, k) in CHECK_NAMES.items():
                    if re.match(pattern, self['DisplayName']) != None:
                        self['CheckName'] = k
                        break

                return self['CheckName']
        except KeyError:
            raise KeyError(key)

        raise KeyError(key)

def correct_app_keys(key):
    try:
        key['DisplayName']
    except KeyError:
        return False

    return True

def get_all_apps(key):
    return list(map(AppDict,
                    filter(
                        correct_app_keys,
                        mapkey(key,
                               lambda x: mapvalue(winreg.OpenKey(key, x)))
                    )
           ))

def compare_version(app_dict):
    print_version(app_dict)
    if app_dict['LatestVersion'] != None \
        and app_dict['LatestVersion'] != app_dict['CheckVersion']:
        return False
    return True

def print_version(app):
    print("{}: {} -> ".format(app['DisplayName'], app['CheckVersion']),
          end='', flush=True)
    print(app['LatestVersion'])

KEY_PATHS = [
    "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
    "SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall"
]

all_apps = []
for key_path in KEY_PATHS:
    try:
        all_apps += get_all_apps(
                        winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, key_path))
        all_apps += get_all_apps(
                        winreg.OpenKey(winreg.HKEY_CURRENT_USER, key_path))
    except FileNotFoundError:
        pass

support_apps = list(filter(lambda x: x['CheckName'] != None, all_apps))
outdate = list(filter(lambda x: not compare_version(x), support_apps))

print("\nOutdated apps:")

for app in outdate:
    print_version(app)

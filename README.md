# Latest version
Collect latest version of softwares on windows

# How to use
Request `http://version.c0ldcat.cc/{PACKAGE_NAME}`

# How to add package
Add check shell script with the package name under `packages` dir. The script output the latest version.

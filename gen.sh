#! /bin/sh

SCRIPT_PATH='packages'
OUT_PATH='out'

[ -d $OUT_PATH ]&&rm -r $OUT_PATH
cp -r out_template $OUT_PATH

packages=$(ls $SCRIPT_PATH)

for pkg_script in $packages
do
    echo $pkg_script
    sh $SCRIPT_PATH/$pkg_script | tr -d "\n" > $OUT_PATH/$pkg_script
done
